package ru.stackprod.synthesis;

/**
 * Created by Руслан on 08.04.2016.
 */
public class Pattern {
    static int LOW = 0; //палец выпрямлен
    static int MIDDLE = 1; //палец наполовину согнут
    static int HIGH = 2; //палец полностью согнут


    public int[] getFingersInstances() {
        return fingersInstances;
    }

    public String getPhrase() {
        return phrase;
    }

    public boolean isRoundDetected() {
        return isRoundDetected;
    }

    int[] fingersInstances;
    String phrase;
    boolean isRoundDetected;



    String emotion;

    public Pattern(int[] fingersInstances, String phrase, boolean isRoundDetected){
        this.fingersInstances = fingersInstances;
        this.phrase = phrase;
        this.isRoundDetected = isRoundDetected;

    }
}
