package ru.stackprod.synthesis;

/**
 * Created by Руслан on 08.04.2016.
 */

public class Intervals {
    public int getMinimumMiddle() {
        return minimumMiddle;
    }

    public int getMiddleMaximum() {
        return middleMaximum;
    }

    private int minimumMiddle, middleMaximum;
    public Intervals(int minimumMiddle, int middleMaximum){
        this.middleMaximum = middleMaximum;
        this.minimumMiddle = minimumMiddle;
    }
}
