package ru.stackprod.synthesis;

import android.media.AudioManager;
import android.media.MediaPlayer;

import java.io.File;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Руслан on 08.04.2016.
 */
public class Player {

    private static MediaPlayer mPlayer;


    public static void play(String url){
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mPlayer.setDataSource(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mPlayer.start();

    }

    public static void stop(){
        if(mPlayer!=null && mPlayer.isPlaying()){
            mPlayer.stop();
        }
    }

}
