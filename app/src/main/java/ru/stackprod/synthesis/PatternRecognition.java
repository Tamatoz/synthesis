package ru.stackprod.synthesis;

import java.util.Arrays;

/**
 * Created by Руслан on 08.04.2016.
 */
//класс для распознавания слова
public class PatternRecognition {
    Intervals[] intervals = new Intervals[5];
    public PatternRecognition(int[] min, int[] max){ //массивы минимальных и максимальных значений сгиба каждого пальца
        if(min.length != 5)
            throw new Error("Введите значения минимума для 5 пальцев");
        if(max.length != 5)
            throw new Error("Введите значения максимума для 5 пальцев");
        for(int i = 0; i < 5; i++){
            intervals[i] = new Intervals(
                    (int) (max[i] - min[i] * 0.33),
                    (int) (max[i] - min[i] * 0.66)
            );
        }
    }

    public Pattern recognizePattern(int[] fingerInstances, boolean isRoundDetected){
        if(fingerInstances.length != 5)
            throw new Error("Введите значения для 5 пальцев");
        int[] instances = new int[5];
        for(int i = 0; i < 5; i++){
            if(fingerInstances[i] < intervals[i].getMinimumMiddle())
                instances[i] = Pattern.HIGH;
            else if(fingerInstances[i] < intervals[i].getMiddleMaximum())
                instances[i] = Pattern.MIDDLE;
            else
                instances[i] = Pattern.LOW;
        }
        for(Pattern pat : Patterns.patterns){
            if((Arrays.equals(pat.getFingersInstances(), (instances)))&&(pat.isRoundDetected() == isRoundDetected))
                return pat;
        }
        return null;
    }
}
