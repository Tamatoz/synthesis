package ru.stackprod.synthesis;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText field = (EditText) findViewById(R.id.editText);
        RadioButton evil = (RadioButton) findViewById(R.id.evil);
        final RadioButton good = (RadioButton) findViewById(R.id.good);
        final RadioButton neutral = (RadioButton) findViewById(R.id.neutral);
        Button say = (Button) findViewById(R.id.say);
        say.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = field.getText().toString();
                String emotion = Vocalizer.EMOTION_EVIL;
                if(good.isChecked())
                    emotion = Vocalizer.EMOTION_GOOD;
                else if(neutral.isChecked())
                    emotion = Vocalizer.EMOTION_NEUTRAL;
                Vocalizer vocalizer = new Vocalizer();
                vocalizer.start(text, Vocalizer.WOMEN, emotion);
            }
        });



        Vocalizer vocalizer = new Vocalizer();
        PatternRecognition recognition = new PatternRecognition(
                new int[]{100, 100, 100, 100, 100}, //Минимальные значения сгиба для каждого пальца
                new int[]{700, 700, 700, 700, 700}  //Максимальные значения сгиба для каждого пальца
        );
        Pattern pat = recognition.recognizePattern(
                new int[]{132, 130, 100, 114, 110}, //Распознаем фразу на основе данных о сгибе пальцев
                false                                //и распознанном круге
        );
        //воспроизводим звук, первый параметр - текст, который нужно озвучить
        //второй параметр - голос озвучки, на выбор Vocalizer.MEN или Vocalizer.WOMEN
        //третий параметр - эмоция озвучки, на выбор EMOTION_EVIL, EMOTION_GOOD, EMOTION_NEUTRAL
        if(pat != null)
            vocalizer.start(pat.getPhrase(), Vocalizer.MEN, Vocalizer.EMOTION_EVIL);
    }


}
