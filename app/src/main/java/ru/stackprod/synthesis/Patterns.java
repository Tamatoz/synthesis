package ru.stackprod.synthesis;

/**
 * Created by Руслан on 08.04.2016.
 */
//Класс, в котором собраны все распознаваемые фразы
public class Patterns {
    public static Pattern[] patterns = new Pattern[]{
            new Pattern(
                    new int[]{Pattern.HIGH, //Указываем уровень согнутости для каждого пальца
                            Pattern.HIGH,
                            Pattern.HIGH,
                            Pattern.HIGH,
                            Pattern.HIGH},
                    "Привет", //Фраза, которую необходимо произнести
                    false //Должен ли быть распознан круг
            )
    };
}
