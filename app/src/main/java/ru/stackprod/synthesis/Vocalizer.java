package ru.stackprod.synthesis;

import android.content.Context;


import java.net.URLEncoder;

/**
 * Created by Руслан on 08.04.2016.
 */
public class Vocalizer  {
    public static int MEN = 0, WOMEN = 1;
    public static String EMOTION_GOOD = "good", EMOTION_NEUTRAL = "neutral", EMOTION_EVIL = "evil";

    public void start(String text, int speaker, String emotion){
        String person = "jane";
        if(speaker == 0){
            person = "zahar";
        }
        Player.play("https://tts.voicetech.yandex.net/generate?" +
                "text=\"" + URLEncoder.encode(text) +  "\"&" +
                "format=mp3&" +
                "lang=ru-RU&" +
                "speaker=" + person + "&" +
                "emotion=" + emotion + "&" +
                "key=76ae7d52-0a49-4800-97a1-5dd57940e907");
    }

}
